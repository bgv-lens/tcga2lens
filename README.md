# tcga2lens

A set of utilities to convert large pan-cancer data from TCGA into tumor-type-specific files that can be utilized by LENS for neoantigen prioritization. 