#!/usr/bin/env python

import argparse
import gzip
import numpy as np
from pprint import pprint

def get_args():
    parser = argparse.ArgumentParser()

    subparsers = parser.add_subparsers(dest='command')
   
    parser_add_tumor_type = subparsers.add_parser('summarize-transcript-expression')
    parser_add_tumor_type.add_argument('-t', '--tx-file', required=True) 
    parser_add_tumor_type.add_argument('-m', '--tumor-type-map', required=True)
    parser_add_tumor_type.add_argument('-o', '--output', required=True)
    
    parser_summarize_nonsilent_mutations = subparsers.add_parser('summarize-nonsilent-mutations')
    parser_summarize_nonsilent_mutations.add_argument('-s', '--nonsilent-file', required=True) 
    parser_summarize_nonsilent_mutations.add_argument('-m', '--tumor-type-map', required=True)
    parser_summarize_nonsilent_mutations.add_argument('-o', '--output', required=True)
    
    return parser.parse_args() 

def summarize_nonsilent_mutations(args):
    """
    """



    tumor_codes = parse_tissue_site_codes(args.tumor_type_map)
    print(set(tumor_codes.values()))


    samp_col_idx_map = {}
    samp_tumor_map = {}
    
    with open(args.output, 'w') as ofo:
        ofo.write("identifier\ttumor_type\ttotal_tcga_samples\tcount_with_variant\tproportion_with_variant\tmean_nz\tmedian_nz\n")
        with gzip.open(args.nonsilent_file, 'rt') as fo:
            for line_idx, line in enumerate(fo.readlines()):
                if line_idx == 0:
                    line = line.rstrip().split('\t')
                    for entry_idx, entry in enumerate(line):
                        if entry != 'sample':
                            code = entry.split('-')[1]
                            samp_col_idx_map[entry_idx] = entry
                            samp_tumor_map[entry] = tumor_codes[code]
                else:
                    line = line.rstrip().split('\t')
                    identifier = ''
                    bufr = {}
                    for val_idx, val in enumerate(line):
                        if val_idx == 0:
                            identifier = val
#                            summary[identifier] = {}
                        else:
                            samp = samp_col_idx_map[val_idx]
                            tumor_type = samp_tumor_map[samp]
                            if tumor_type in bufr.keys():
                                bufr[tumor_type].append(float(val))
                            else:
                                bufr[tumor_type] = [float(val)]
                    for k, v in bufr.items():
                        total = len(v)
                        
#                        summary[identifier][k] = {}
                        mean = np.mean(v)
                        nz = [int(x) for x in v if x != 0]
                        median = np.median(v)
                        mean_nz = 'NA'
                        median_nz = 'NA'
                        if nz:
                            mean_nz = np.mean([nz])
                            median_nz = np.median([nz])
                     
                        ofo.write("{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(identifier, k, total, len(nz), (float(len(nz))/float(total)), mean_nz, median_nz))


#        for identifier in summary.keys():
#            for tumor_type in summary[identifier].keys():
#                print(identifier)
#                print(tumor_type)
#
#                mean = summary[identifier][tumor_type]['mean']
#                median = summary[identifier][tumor_type]['median']
#                mean_nz = summary[identifier][tumor_type]['mean_nz']
#                median_nz = summary[identifier][tumor_type]['median_nz']

def summarize_transcript_expression(args):
    """
    """


    tumor_codes = parse_tissue_site_codes(args.tumor_type_map)
    print(set(tumor_codes.values()))


    samp_col_idx_map = {}
    samp_tumor_map = {}
    
#    avg_exp_by_tumor_type = {}

    with open(args.output, 'w') as ofo:
        ofo.write("identifier\ttumor_type\tmean\tmedian\tmax\tiqr\n")
        with gzip.open(args.tx_file, 'rt') as fo:
            for line_idx, line in enumerate(fo.readlines()):
                if line_idx == 0:
                    line = line.rstrip().split('\t')
                    for entry_idx, entry in enumerate(line):
                        if entry != 'sample':
    #                        print(entry)
                            code = entry.split('-')[1]
                            samp_col_idx_map[entry_idx] = entry
                            samp_tumor_map[entry] = tumor_codes[code]
                else:
                    line = line.rstrip().split('\t')
                    identifier = ''
                    bufr = {}
                    for val_idx, val in enumerate(line):
                        if val_idx == 0:
                            identifier = val
#                            summary[identifier] = {}
                        else:
                            samp = samp_col_idx_map[val_idx]
                            tumor_type = samp_tumor_map[samp]
                            if tumor_type in bufr.keys():
                                bufr[tumor_type].append(float(val))
                            else:
                                bufr[tumor_type] = [float(val)]
                    for k, v in bufr.items():
#                        summary[identifier][k] = {}
                        mean = np.mean(v)
                        median = np.median(v)
                        max = np.max(v)
                        q75, q25 = np.percentile(v, [75 ,25])
                        iqr = q75 - q25
#                        summary[identifier][k]['mean'] = mean
#                        summary[identifier][k]['median'] = median
#                        summary[identifier][k]['max'] = max
#                        summary[identifier][k]['iqr'] = iqr
                        ofo.write("{}\t{}\t{}\t{}\t{}\t{}\n".format(identifier, k, mean, median, max, iqr))
#                    if k in avg_exp_by_tumor_type.keys():
#                        avg_exp_by_tumor_type[k].append(mean)
#                    else:
#                        avg_exp_by_tumor_type[k] = [mean]


#    with open(args.output, 'w') as ofo:
#        ofo.write("identifier\ttumor_type\tmean\tmedian\tmax\tiqr\tpercentile_within_tumor_type\n")
#        ofo.write("identifier\ttumor_type\tmean\tmedian\tmax\tiqr\n")
#        for identifier in summary.keys():
#            for tumor_type in summary[identifier].keys():
#                print(identifier)
#                print(tumor_type)

#                mean = summary[identifier][tumor_type]['mean']
#                median = summary[identifier][tumor_type]['median']
#                max = summary[identifier][tumor_type]['max']
#                iqr = summary[identifier][tumor_type]['iqr']

#                ofo.write("{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(identifier, tumor_type, mean, median, max, iqr, sum(avg_exp_by_tumor_type[tumor_type] < mean) / float(len(avg_exp_by_tumor_type[tumor_type]))))
#                ofo.write("{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(identifier, tumor_type, mean, median, max, iqr))



def parse_tissue_site_codes(tssc):

    tissue_codes = {}

    with open(tssc) as fo:
        for line in fo.readlines():
            line = line.split('\t')
            code = line[0] 
            tissue = line[2]
            tissue_codes[code] = tissue
    return tissue_codes


    


def main():
   args = get_args()
   
   print(args)

   if args.command == 'add-tumor-type':
       add_tumor_type(args)
   if args.command == 'summarize-transcript-expression':
       summarize_transcript_expression(args)
   if args.command == 'summarize-nonsilent-mutations':
       summarize_nonsilent_mutations(args)

if __name__ == '__main__':
    main()
